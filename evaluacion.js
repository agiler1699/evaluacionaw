function registrarCliente(request, res) {
    //Primero vamos a convertir el string de datos que recibimos a un json para poder utilizarlo
    var entradaString = JSON.stringify(request.body);
    var entrada = JSON.parse(entradaString);

    //Ahora validamos que si hayamos recibido todos los campos

    if (!entrada['cedula'] || !entrada['apellidos'] || !entrada['nombres'] || !entrada['direccion'] || !entrada['telefono']) {
        return res.status(400).send({ 'message': 'Faltan campos' });
    }

    //Ahora vamos a crear un sql con los datos del cliente que vamos a ingresar el cual vamos a usar para hacer el ingreso en la base de datos
    const queryCrearCliente = `insert 
      into clientes (cedula, apellidos, nombres, direccion, telefono)
      values('${entrada['cedula']}', '${entrada['apellidos']}', '${entrada['nombres']}', '${entrada['direccion']}', '${entrada['telefono']}') returning *;`
    try {
        //Aqui hacemos el ingreso a la base de datos 
        db.query(queryCrearCliente).then((datos) => {
            //Se crea el client correctamente y enviamos la respuesta de que fue creada
            return res.status(200).json({ "message": "Cliente creado correctamente" });

        }).catch((error) => {
            //Ocurrio un error y enviamos la respuesta indicada
            if (error.routine === '_bt_check_unique') {
                return res.status(400).send({ 'message': 'Ocurrio un error al ingresar los datos' })
            }
            return res.status(400).send(error);
        })
    } catch (error) {
        //Ocurrio un error ya que no habian todos los campos necesarios
        if (error.routine === '_bt_check_unique') {
            return res.status(400).send({ 'message': 'Se repite alguno de sus datos' })
        }
        return res.status(400).send(error);
    }
}
